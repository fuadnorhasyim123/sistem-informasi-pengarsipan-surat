@extends('layouts.app')

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>About</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Data Arsip -->
    <section class="section">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{asset('profile.jpg')}}" width="120" height="150" alt="">
                    </div>
                    <div class="col-md-8">
                        Aplikasi Ini Dibuat Oleh : <br>
                        Nama : M. Fuad Noor Hasyim <br>
                        NIM : 1931713047 <br>
                        Tanggal : 22 November 2021
                    </div>
                </div>
            </div>
            <div class="card-body">
            </div>
        </div>

    </section>
</div>
@endsection
@push('scripts')
@endpush