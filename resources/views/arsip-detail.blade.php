@extends('layouts.app')

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Detail Arsip</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('arsip')}}">Arsip</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Detail</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Data Arsip -->
    <section class="section">
        <div class="card">
            <div class="card-header">
                Nomor Surat : {{$arsip->arsip_nomor}} <br>
                Kategori : {{$arsip->arsip_kategori}} <br>
                Judul : {{$arsip->arsip_judul}} <br>
                Waktu Unggah : {{$arsip->created_at}}
            </div>
            <div class="card-body">
                <iframe src="{{asset('file/'.$arsip->arsip_file)}}" width="100%" height="500"></iframe>

                <div class="buttons">
                    <a href="{{route('arsip')}}" class="btn btn-secondary"> Kembali</a>
                    <a href="/arsip/download/{{$arsip->arsip_file}}" class="btn btn-warning"> Unduh</a>
                    <a href="javascript:void(0)" class="btn btn-primary" id="btnEdit" data-toggle="modal" data-id="{{ $arsip->arsip_id }} "> Edit</a>
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                </div>
            </div>
        </div>

    </section>

    <!-- modal edit -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editLabel"></h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('arsip.edit')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="arsip_id" id="arsip_id">
                    @csrf
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="ns" class=" form-control-label">Nomor Surat</label>
                                <input type="text" id="arsip_nomor" name="arsip_nomor" placeholder="Masukkan Nomor Surat" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="k" class=" form-control-label">Kategori</label>
                                <select name="arsip_kategori" id="arsip_kategori" class="form-control">
                                    <option>---</option>
                                    <option value="Undangan">Undangan</option>
                                    <option value="Pengumuman">Pengumuman</option>
                                    <option value="Nota Dinas">Nota Dinas</option>
                                    <option value="Pemberitahuan">Pemberitahuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="j" class=" form-control-label">Judul</label>
                                <input type="text" id="arsip_judul" name="arsip_judul" placeholder="Masukkan Judul" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="f" class=" form-control-label">File Arsip (*)</label>
                                <input type="text" value="{{$arsip->arsip_file}}" class="form-control" readonly>
                                <input type="file" id="arsip_file" name="arsip_file" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->
</div>
@endsection
@push('scripts')
<script>
    $('body').on('click', '#btnEdit', function() {
        var data_id = $(this).data('id');
        $.get('/arsip/edit/' + data_id, function(data) {
            $('#editLabel').html("Edit Arsip");
            $('#btn-save').prop('disabled', false);
            $('#edit').modal('show');
            $('#arsip_id').val(data.arsip_id);
            $('#arsip_nomor').val(data.arsip_nomor);
            $('#arsip_kategori').val(data.arsip_kategori);
            $('#arsip_judul').val(data.arsip_judul);
            $('#arsip_file').val(data.arsip_file);
        })
    });
</script>
@endpush