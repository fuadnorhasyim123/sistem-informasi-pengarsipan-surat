@extends('layouts.app')

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Arsip</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Arsip</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Basic Tables start -->
    <section class="section">
        <div class="card">
            <div class="card-header">
                Data Arsip Surat
            </div>
            <div class="card-body">
                <div class="col-md-6 mb-2">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnTambah" data-toggle="modal"> Arsipkan Surat</a>
                </div>
                <table class="table" id="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Surat</th>
                            <th>Kategori</th>
                            <th>Judul</th>
                            <th>Waktu Pengarsipan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        @foreach ($arsip as $data)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{ $data->arsip_nomor }}</td>
                            <td>{{ $data->arsip_kategori }}</td>
                            <td>{{ $data->arsip_judul }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>
                                <a href="arsip/lihat/{{$data->arsip_id}}" class="btn btn-primary"> Lihat</a>
                                <a href="/arsip/download/{{$data->arsip_file}}" class="btn btn-warning"> Unduh</a>
                                <a href="javascript:void(0)" class="btn btn-danger" id="btnHapus" data-toggle="modal" data-id="{{ $data->arsip_id }} "> Hapus</a>
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    <!-- modal tambah -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahLabel"></h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('arsip.tambah')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="ns" class=" form-control-label">Nomor Surat</label>
                                <input type="text" id="arsip_nomor" name="arsip_nomor" placeholder="Masukkan Nomor Surat" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="k" class=" form-control-label">Kategori</label>
                                <select name="arsip_kategori" id="arsip_kategori" class="form-control">
                                    <option>---</option>
                                    <option value="Undangan">Undangan</option>
                                    <option value="Pengumuman">Pengumuman</option>
                                    <option value="Nota Dinas">Nota Dinas</option>
                                    <option value="Pemberitahuan">Pemberitahuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="j" class=" form-control-label">Judul</label>
                                <input type="text" id="arsip_judul" name="arsip_judul" placeholder="Masukkan Judul" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="f" class=" form-control-label">File Surat</label>
                                <input type="file" id="arsip_file" name="arsip_file" class="form-control" accept="application/pdf" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal hapus -->
    <div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="hapusLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="hapusLabel"></h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('arsip.hapus')}}" method="POST">
                    @csrf
                    <input type="hidden" name="arsip_id" id="arsip_id">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary">Ya</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->
</div>
@endsection
@push('scripts')
<script>
    // Jquery Datatable
    let datatable = $("#table").DataTable()

    // modal tambah
    $(document).ready(function() {
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Tambah Arsip");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#arsip_nomor').val("");
            $('#arsip_kategori').val("---");
            $('#arsip_judul').val("");
            $('#arsip_file').val("");
        });
    });

    // modal hapus
    $('body').on('click', '#btnHapus', function() {
        var data_id = $(this).data('id');
        $.get('/arsip/edit/' + data_id, function(data) {
            $('#hapusLabel').html("Hapus Arsip");
            $('#btn-save').prop('disabled', false);
            $('#hapus').modal('show');
            $('#arsip_id').val(data.arsip_id);
        })
    });
</script>
@endpush