<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;
use Illuminate\Support\Facades\File;
use Response;

class ArsipController extends Controller
{
    // arsip view
    public function index()
    {
        // get data arsip
        $arsip = DB::table('arsip')->get();

        return view('arsip', [
            "arsip" => $arsip
        ]);
    }

    // arsip detail
    public function detail($id)
    {
        // get detail arsip
        $arsip = DB::table('arsip')->where('arsip_id', $id)->first();

        return view('arsip-detail', [
            "arsip" => $arsip
        ]);
    }

    // tambah arsip
    public function store(Request $request)
    {
        // upload file
        $file = $request->file('arsip_file');
        $tujuan_upload = 'file/'; //nama folder
        $file->move($tujuan_upload, $file->getClientOriginalName());

        // query insert
        DB::table('arsip')->insert([
            'arsip_nomor'    => $request->arsip_nomor,
            'arsip_kategori'     => $request->arsip_kategori,
            'arsip_judul'     => $request->arsip_judul,
            'arsip_file'         => $file->getClientOriginalName()
        ]);

        Alert::success('Sukses', 'Data Berhasil Ditambah');
        return redirect("/arsip");
    }

    // update arsip
    public function update(Request $request)
    {
        // upload file
        $dataFile = $request->arsip_file;
        if ($dataFile != "") {
            $file = $request->file('arsip_file');
            $tujuan_upload = 'file/'; //nama folder
            $file->move($tujuan_upload, $file->getClientOriginalName());
        }

        // query update
        if ($dataFile == "") {
            DB::table('arsip')->where('arsip_id', $request->arsip_id)->update([
                'arsip_nomor'    => $request->arsip_nomor,
                'arsip_kategori'     => $request->arsip_kategori,
                'arsip_judul'     => $request->arsip_judul
            ]);
        } else {
            DB::table('arsip')->where('arsip_id', $request->arsip_id)->update([
                'arsip_nomor'    => $request->arsip_nomor,
                'arsip_kategori'     => $request->arsip_kategori,
                'arsip_judul'     => $request->arsip_judul,
                'arsip_file'         => $file->getClientOriginalName()
            ]);
        }

        Alert::success('Sukses', 'Data Berhasil Diedit');
        return redirect("/arsip");
    }

    // edit arsip
    public function edit($id)
    {
        // get arsip where id
        $arsip = DB::table('arsip')->where('arsip_id', $id)->first();

        return Response::json($arsip);
    }

    // hapus arsip
    public function destroy(Request $request)
    {
        // delete file
        $arsip = DB::table('arsip')->where('arsip_id', $request->arsip_id)->first();
        File::delete('file/' . $arsip->arsip_file);

        // query delete
        DB::table('arsip')->where('arsip_id', $request->arsip_id)->delete();


        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/arsip");
    }

    // download file
    public function download($id)
    {
        return Response::download("file/" . $id);
    }
}
