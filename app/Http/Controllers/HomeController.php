<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // dashboard view
    public function index()
    {
        $user = DB::table('users')->count();
        $arsip = DB::table('arsip')->count();

        return view('home', [
            "user"  =>  $user,
            "arsip"  =>  $arsip
        ]);
    }
}
