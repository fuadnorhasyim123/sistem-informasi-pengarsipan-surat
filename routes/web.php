<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login
Route::get('/', function () {
    return view('auth.login');
});

// routes auth
Auth::routes();

Route::middleware(['auth'])->group(function () {
    // route home
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    // route arsip
    Route::get('/arsip', [App\Http\Controllers\ArsipController::class, 'index'])->name('arsip');
    Route::post('/arsip/tambah', [App\Http\Controllers\ArsipController::class, 'store'])->name('arsip.tambah');
    Route::get('/arsip/download/{id}', [App\Http\Controllers\ArsipController::class, 'download']);
    Route::post('/arsip/edit', [App\Http\Controllers\ArsipController::class, 'update'])->name('arsip.edit');
    Route::get('/arsip/edit/{id}', [App\Http\Controllers\ArsipController::class, 'edit']);
    Route::post('/arsip/hapus', [App\Http\Controllers\ArsipController::class, 'destroy'])->name('arsip.hapus');
    Route::get('/arsip/lihat/{id}', [App\Http\Controllers\ArsipController::class, 'detail']);

    // route about
    Route::get('/about', [App\Http\Controllers\AboutController::class, 'index'])->name('about');
});
